Quickstart
==========

Installation
------------

Simple:

* ``pip install django-password-reset``

Usage
-----

Simple:

* Add ``password_reset`` to your ``INSTALLED_APPS``

* ``include('password_reset.urls')`` in your root ``urls.py``

* Link to the password reset page: ``{% url "password_reset_recover" %}``

* Create a ``password_reset/base.html`` template and adapt it to your site's
  structure

# Base.html #

{% extends "base-gentelella-sin-menu.html" %}
{% block css %}

{% endblock %}
{% block js %}

{% endblock %}
{% block title %}

{% endblock %}

{% block content %}
{% endblock %}